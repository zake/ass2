package MonashBook.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.0.v20130507-rNA", date="2013-10-26T14:29:15")
@StaticMetamodel(UserGroup.class)
public class UserGroup_ { 

    public static volatile SingularAttribute<UserGroup, Long> id;
    public static volatile SingularAttribute<UserGroup, String> username;
    public static volatile SingularAttribute<UserGroup, String> groupName;

}